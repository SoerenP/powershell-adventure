Add-Type -AssemblyName System.Web
Add-Type -AssemblyName System.Security
Add-Type -AssemblyName System.Core

if($Host.UI.RawUI.WindowTitle -like "*administrator*")
{
    $Host.UI.RawUI.ForegroundColor = "Red"
}

function Easy-View { process {$_; Start-Sleep -seconds .5}}

function Find-And-Replace-In-File($filepath, $target, $replace){
    $content = Get-Content $filepath
    $content -replace $target, $replace | Set-Content $filepath
}

#Usage: Write-note "#Jira-1234" then write your notes for current task, you can then always grep it later. 
function Write-Note([Parameter(Mandatory=$true)][string]$tag) {
        
    $newcontent = (Get-Date).ToString() + " --- " + $tag 
    $notefilePath = "C:\Users\sep\Desktop\Notes.txt"
    
    #Dirty way of prepending, read it all and concatenate, then set. Pipe get content to out-string, because it returns an array of stirngs?!?!?! does not preserve newlines
    ("`n `n" + $newcontent) + (Get-Content $notefilePath | out-string) | Set-Content $notefilePath
    notepad $notefilePath    

}

function Touch($filename)
{
    New-Item -ItemType file $filename
}

function Get-Assembly-Version($filepath) {
    [Reflection.AssemblyName]::GetAssemblyName($filepath).Version
}

function Get-Reload-Profile {
    . $profile
}

function Get-Rec-Grep($filePattern, $grepTarget)
{
    ls $filePattern -r | sls $grepTarget
}

function Get-Regex($regex){
    [Regex]::Matches($_, $regex, 'IgnoreCase') | % { $_.Value }
}

function Remove-Whitespace-Newlines([string]$str){
    $str -replace " ", "" -replace "\n", ""
}

function Get-Sha256($input){
    #[convert]::ToBase64String([cryptography]::sha256.create().computehash([text.encoding]::ascii.getstring($input)))	
    $stringAsStream = [System.IO.MemoryStream]::new()
    $writer = [System.IO.StreamWriter]::new($stringAsStream)
    $writer.Write($input)
    $writer.Flush()
    $stringAsStream.Position = 0
    Get-FileHash -InputStream $stringAsStream -Algorithm SHA256 | Select-Object Hash
}

function FromBase64([string]$str) {
    [text.encoding]::utf8.getstring([convert]::FromBase64String($str))
}

function ToBase64([string]$str) {
    [convert]::ToBase64String([text.encoding]::utf8.getBytes($str))
}

function ToBase64Url([string]$str) {
    (ToBase64($str)) -replace "=", '' -replace "\+", "-" -replace "/", "_"
}

function FromBase64Url([string]$str) {
    $str = $str -replace "-", "\+" -replace "_", "/"

    switch ($str.Length % 4) {
        0 { break }
        2 { $str += '=='; break }
        3 { $str += '='; break }
        Default { Write-Warning 'Illegal base64 url string'}
    }

    FromBase64($str)
}

function Get-Escaped-For-VS([string]$string) {
	$str = ($string -replace '"', '""')
	'@"'+$str+'"'
}

function UrlEncode([string]$url) {
    [Web.Httputility]::UrlEncode($url)
}

function UrlDecode([string]$url) {
    [Web.Httputility]::UrlDecode($url)
}

function HtmlDecode([string]$url){
    [Web.Httputility]::HtmlDecode($url)
}

function HtmlEncode([string]$url){
    [Web.HttpUtility]::HtmlEncode($url)
}

function Run-GoogleQuery {
    Start-Process ('http://www.google.com/search?q=' + (UrlEncode ($args -join " ")))
}

function FromUnixTimeStamp([string]$unix){
	$origin = New-Object -Type DateTime -ArgumentList 1970, 1, 1, 0, 0, 0, 0
	$origin.AddSeconds($unix)
}

function Get-Guid {
    $guid = [guid]::NewGuid().ToString()
    Remove-Whitespace-Newlines($guid)
}

function Get-Guid-Test-Annotation {
    $guid = Get-Guid
    '// %TEST-ID:' + $guid + '%'
}

function Split-Jwt([string]$jwt){
    $split = $jwt.Split(".")
    $header = FromBase64Url $split[0]
    $body = FromBase64Url $split[1]
    [PSCustomObject][ordered]@{
        Header = $header
        Body = $body
    }
}

function Show-Object($object){

    ($object | Get-Member -MemberType *Property).Name | 
        ForEach-Object { 
      
            New-Object PSObject -Property ([Ordered]@{Property=$_; Value=$object.$_ }) 
  
        } | Out-GridView 
}

function Get-Cert([string]$uri){
    $response = Invoke-WebRequest -URI $uri #you have to call it first it seems, otherwise service point wont find cert? weird.
    $servicePoint = [System.Net.ServicePointManager]::FindServicePoint([Uri]::New($uri))
    $servicePoint.Certificate
}

Set-Alias gq Run-GoogleQuery
